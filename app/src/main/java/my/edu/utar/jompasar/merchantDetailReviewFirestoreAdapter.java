package my.edu.utar.jompasar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.Timestamp;


import java.text.SimpleDateFormat;
import java.util.Date;

public class merchantDetailReviewFirestoreAdapter extends FirestoreRecyclerAdapter<review_model, merchantDetailReviewFirestoreAdapter.reviewViewHolder> {
    Context context;

    public merchantDetailReviewFirestoreAdapter(Context c, @NonNull FirestoreRecyclerOptions<review_model> options) {
        super(options);
        this.context = c;
    }

    @NonNull
    @Override
    public reviewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.merchant_detail_review_tab_item,parent,false);
        return new reviewViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull reviewViewHolder holder, int position, @NonNull review_model model) {
        //Display the Data
        String star = "Error showing rating";
        if(model.getRating()==5){
            star = "★★★★★";
        }else if(model.getRating()==4){
            star = "★★★★☆";
        }else if(model.getRating()==3){
            star = "★★★☆☆";
        }else if(model.getRating()==2){
            star = "★★☆☆☆";
        }else if(model.getRating()==1){
            star = "★☆☆☆☆";
        }
        holder.review_rating.setText(star);
        holder.review_buyer_name.setText(model.getBuyer_name());
        holder.review_comment.setText(String.valueOf(model.getComment()));
        Timestamp timestamp = model.getTimestamp();
        Date date = timestamp.toDate();
        String times = new SimpleDateFormat("yyyy-MM-dd").format(date);
        holder.review_timestamp.setText(times);

    }

    public class reviewViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {

        private TextView review_rating;
        private TextView review_buyer_name;
        private TextView review_comment;
        private TextView review_timestamp;

        public reviewViewHolder(@NonNull View itemView) {
            super(itemView);

            //XML hook to Java Code;
            review_rating = itemView.findViewById(R.id.rating);
            review_buyer_name = itemView.findViewById(R.id.buyer_name);
            review_comment = itemView.findViewById(R.id.comment);
            review_timestamp = itemView.findViewById(R.id.r_timestamp);
        }

        @Override
        public void onClick(View v) {

        }
    }

}
