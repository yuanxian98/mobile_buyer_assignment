package my.edu.utar.jompasar;

import java.io.Serializable;
import java.util.ArrayList;

public class seller_model implements Serializable {
    private String address;
    private String email;
    private String shop_name;
    private String tel_no;
    private String username;
    private String img_url;
    private String desc;
    private String city;
    private Double avg_rating;
    private Integer rating_count;

    private transient ArrayList<seller_menu_model> menu;
    private transient ArrayList<String> rated_buyer;

    public seller_model(){

    }

    public seller_model(String address, String email, String shop_name, String tel_no, String username, String desc, String city, Double avg_rating, ArrayList<String> rated_buyer, ArrayList<seller_menu_model> menu, Integer rating_count) {
        this.address = address;
        this.email = email;
        this.shop_name = shop_name;
        this.tel_no = tel_no;
        this.username = username;
        this.avg_rating = avg_rating;
        this.rating_count = rating_count;
        this.menu = menu;
        this.rated_buyer = rated_buyer;
        this.desc =desc;
        this.city=city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getTel_no() {
        return tel_no;
    }

    public void setTel_no(String tel_no) {
        this.tel_no = tel_no;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getRating_count() {
        return rating_count;
    }

    public void setRating_count(Integer rating_count) {
        this.rating_count = rating_count;
    }

    public ArrayList<seller_menu_model> getMenu() {
        return menu;
    }

    public void setMenu(ArrayList<seller_menu_model> menu) {
        this.menu = menu;
    }

    public Double getAvg_rating() {
        return avg_rating;
    }

    public void setAvg_rating(Double avg_rating) {
        this.avg_rating = avg_rating;
    }

    public ArrayList<String> getRated_buyer() {
        return rated_buyer;
    }

    public void setRated_buyer(ArrayList<String> rated_buyer) {
        this.rated_buyer = rated_buyer;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
