package my.edu.utar.jompasar;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class merchantInfoAdapter extends FragmentPagerAdapter {
    private Context context;
    int totalTabs;

    public merchantInfoAdapter(@NonNull FragmentManager fm, Context context, int totalTabs) {
        super(fm);
        this.context = context;
        this.totalTabs = totalTabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch( position ){
            case 0:
                merchantDetailDetailsTabFragment tab0 = new merchantDetailDetailsTabFragment();
                return tab0;
            case 1:
                merchantDetailReviewTabFragment tab1 = new merchantDetailReviewTabFragment();
                return tab1;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return totalTabs;
    }
}
