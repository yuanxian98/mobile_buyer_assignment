package my.edu.utar.jompasar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.Timestamp;

import java.text.SimpleDateFormat;
import java.util.Date;

public class orderHistoryFirestoreAdapter extends FirestoreRecyclerAdapter<order_model, orderHistoryFirestoreAdapter.orderHistoryViewHolder>{
    Context context;
    private OnListItemClick onListItemClick;

    public orderHistoryFirestoreAdapter(Context c, @NonNull FirestoreRecyclerOptions<order_model> options, OnListItemClick onListItemClick) {
        super(options);
        this.onListItemClick = onListItemClick;
        this.context = c;
    }

    @NonNull
    @Override
    public orderHistoryFirestoreAdapter.orderHistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_history_list_item,parent,false);
        return new orderHistoryFirestoreAdapter.orderHistoryViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull orderHistoryViewHolder holder, int position, @NonNull order_model model) {

        holder.merchantName.setText(model.getSeller_name());
        Timestamp timestamp = model.getOrder_time();
        Date date = timestamp.toDate();
        String month_day = new SimpleDateFormat("MMMM dd").format(date);
        String year = new SimpleDateFormat("YYYY").format(date);
        String time = new SimpleDateFormat("hh:mm a").format(date);
        String full_date = month_day + ", " + year + " at " + time;
        holder.orderTime.setText(full_date);
        holder.orderStatus.setText(model.getStatus());
    }

    public class orderHistoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView merchantName, orderTime, orderStatus;

        public orderHistoryViewHolder(View view) {
            super(view);
            merchantName = view.findViewById(R.id.merchant_name);
            orderTime = view.findViewById(R.id.order_time);
            orderStatus = view.findViewById(R.id.order_status);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onListItemClick.onItemClick(getSnapshots().getSnapshot(getAdapterPosition()).getId());
        }
    }

    //Interface used for the MainActivity
    public interface OnListItemClick {
        //Definition on what sort of data will be passed to MainActivity
        void onItemClick(String orderId);
    }
}
