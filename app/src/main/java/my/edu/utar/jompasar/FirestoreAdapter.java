package my.edu.utar.jompasar;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.firebase.ui.firestore.ObservableSnapshotArray;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.io.Serializable;


public class FirestoreAdapter extends FirestoreRecyclerAdapter<seller_model, FirestoreAdapter.hawkerViewHolder> {

    FirebaseStorage fStore;
    StorageReference storeRef;
    private OnListItemClick onListItemClick;
    Context context;

    public FirestoreAdapter(Context c,@NonNull FirestoreRecyclerOptions<seller_model> options, OnListItemClick onListItemClick) {
        super(options);
        this.onListItemClick = onListItemClick;
        this.context = c;
    }

    @NonNull
    @Override
    public hawkerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hawker_list_item,parent,false);
        return new hawkerViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull hawkerViewHolder holder, int position, @NonNull seller_model model) {
        //Firebase Variable
        storeRef= FirebaseStorage.getInstance().getReference();
        String seller_id = getSnapshots().getSnapshot(position).getId();
        StorageReference display = storeRef.child("/seller/"+ seller_id +"/seller_img.jpg"); // Design Problem? - How to Get every seller display profile ?
        display.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Picasso.get().load(uri).into(holder.hawker_list_img);
            }
        });

        //Display the Data
        holder.hawker_list_name.setText(model.getShop_name());
        holder.hawker_list_desc.setText(model.getDesc());
        holder.hawker_list_rating.setText(String.valueOf(model.getAvg_rating()));

    }

    public class hawkerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView  hawker_list_name;
        private TextView  hawker_list_desc;
        private TextView  hawker_list_rating;
        private ImageView hawker_list_img;

        public hawkerViewHolder(@NonNull View itemView) {
            super(itemView);

            //XML hook to Java Code;
            hawker_list_name = itemView.findViewById(R.id.hawker_list_shop_name);
            hawker_list_desc = itemView.findViewById(R.id.hawker_list_shop_desc);
            hawker_list_rating = itemView.findViewById(R.id.hawker_list_rating_score);
            hawker_list_img = itemView.findViewById(R.id.hawker_imageholder);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            //This will pass the data to MainActivity;
//            onListItemClick.onItemClick(getItem(getAdapterPosition()),getAdapterPosition(), getSnapshots());
            onListItemClick.onItemClick(getSnapshots().getSnapshot(getAdapterPosition()).getId());
        }
    }

    //Interface used for the MainActivity
    public interface OnListItemClick {
        //Definition on what sort of data will be passed to MainActivity
        void onItemClick(String merchantId);
    }
}
