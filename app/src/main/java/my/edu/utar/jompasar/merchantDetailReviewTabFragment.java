package my.edu.utar.jompasar;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class merchantDetailReviewTabFragment extends Fragment {
    //Firebase Variable
    FirebaseFirestore fStore;
    FirebaseAuth fAuth;

    //Firestore UI Variable
    private RecyclerView fireStoreReviewList;
    private merchantDetailReviewFirestoreAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.merchant_detail_review_tab, container, false);

        fAuth = FirebaseAuth.getInstance();
        fireStoreReviewList = root.findViewById(R.id.merchantDetailList);
        String merchantId = getActivity().getIntent().getExtras().getString("merchantId");

        //Query and Variable Initialization
        fStore = FirebaseFirestore.getInstance();
        Query query = fStore.collection("reviews").whereEqualTo("seller_id",merchantId).whereEqualTo("type","seller");
        //Recycler Options
        FirestoreRecyclerOptions options = new FirestoreRecyclerOptions.Builder<review_model>()
                .setQuery(query,review_model.class)
                .build();

        adapter = new merchantDetailReviewFirestoreAdapter(getContext(),options);

        //View Holder Class
        fireStoreReviewList.setHasFixedSize(true);
        fireStoreReviewList.setLayoutManager(new LinearLayoutManager(getActivity()));
        fireStoreReviewList.setAdapter(adapter);

        return root;
    }
    @Override
    public void onStart() {
        super.onStart();
        if (adapter != null) {
            adapter.startListening();
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        if (adapter != null) {
            adapter.stopListening();
        }

    }
}
