package my.edu.utar.jompasar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class signup extends AppCompatActivity {

    //Global Variable
    EditText buyer_signUpUsername,buyer_signupPassword,buyer_signupEmail,buyer_signupTelno, buyer_signup_addr;
    Button submitBtn;
    String buyerUserID;

    //Firebase Variable
    FirebaseAuth fAuth;
    FirebaseFirestore fStore;
    StorageReference storeRef;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        //Bind Java Code with XML Layout
        buyer_signupEmail = findViewById(R.id.buyer_signup_email);
        buyer_signupPassword = findViewById(R.id.buyer_signup_password);
        buyer_signUpUsername = findViewById(R.id.buyer_signup_username);
        buyer_signupTelno = findViewById(R.id.buyer_signup_phoneNo);
        buyer_signup_addr = findViewById(R.id.buyer_signup_addr);
        submitBtn=findViewById(R.id.buyer_signup_submit);


        //Instantiate Firebase Obj
        fAuth =FirebaseAuth.getInstance();
        fStore =FirebaseFirestore.getInstance();
        storeRef = FirebaseStorage.getInstance().getReference();



        //Button Logic
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Get Details
                String buyerEmail = buyer_signupEmail.getText().toString();
                String buyerPassword = buyer_signupPassword.getText().toString();
                String buyerUsername = buyer_signUpUsername.getText().toString();
                String buyerTelno = buyer_signupTelno.getText().toString();
                String buyerAddress = buyer_signup_addr.getText().toString();

                fAuth.createUserWithEmailAndPassword(buyerEmail,buyerPassword).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        Toast.makeText(signup.this, "Account Register Succesfully!", Toast.LENGTH_SHORT).show();

                        //Firestore Code
                        //Getting the User ID and Creating a Document at buyers collection on Firebase
                        buyerUserID = fAuth.getCurrentUser().getUid();
                        DocumentReference docRef = fStore.collection("buyers").document(buyerUserID);

                        //Put Buyers Details to HashMap
                        Map<String,Object> buyerProfile = new HashMap<>();
                        ArrayList<String> rated_seller = new ArrayList<>();
                        buyerProfile.put("user_name",buyerUsername);
                        buyerProfile.put("email",buyerEmail);
                        buyerProfile.put("address",buyerAddress);
                        buyerProfile.put("tel_no",buyerTelno);
                        buyerProfile.put("rating_count",0);
                        buyerProfile.put("average_rating",0);
                        buyerProfile.put("rated_seller",rated_seller);


                        //Setting New Buyer Profile on Firestore Document
                        docRef.set(buyerProfile).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(signup.this, "User Profile is Created!", Toast.LENGTH_SHORT).show();
                                Log.d("ProfileMsg_Success","User Profile is Created!");
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(signup.this, "Failure on Creating User Profile!", Toast.LENGTH_SHORT).show();
                                Log.d("ProfileMsg_Failure","Failure on Creating User Profile");
                            }
                        });

                        //Send User to Login Page after Registering
                        startActivity(new Intent(getApplicationContext(),Login.class));
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(signup.this, "Error on Registering New Account!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }






}