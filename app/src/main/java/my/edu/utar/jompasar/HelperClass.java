package my.edu.utar.jompasar;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class HelperClass {
    public String roundPriceRM(double value) {
        if (2 < 0) throw new IllegalArgumentException();
        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return "RM " + bd.doubleValue();
    }
}
