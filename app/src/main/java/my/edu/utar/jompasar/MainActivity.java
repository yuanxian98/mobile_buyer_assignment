package my.edu.utar.jompasar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.firebase.ui.firestore.ObservableSnapshotArray;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firestore.v1.StructuredQuery;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,FirestoreAdapter.OnListItemClick {

    //Global Variable
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    NavigationView navView;
    String buyerUserID;

    //Firebase Variable
    FirebaseFirestore fStore;
    FirebaseAuth fAuth;
    StorageReference storeRef;


    //Firestore UI Variable
    private RecyclerView fireStoreHawkerList;
    private FirestoreAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Firebase Variable Initialization
        fAuth = FirebaseAuth.getInstance();
        fStore = FirebaseFirestore.getInstance();
        storeRef = FirebaseStorage.getInstance().getReference();
        buyerUserID = fAuth.getCurrentUser().getUid();




        //Drawer UI Logic
        drawerLayout = findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);

        navView=findViewById(R.id.nav_view);
        navView.bringToFront();

        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        navView.setNavigationItemSelectedListener(this);



        //Firestore UI RecycleView Logic
        //Query and Variable Initialization
        fireStoreHawkerList = findViewById(R.id.firestore_hawker_list);
        Query query = fStore.collection("sellers/" );

        //Recycler Options
        FirestoreRecyclerOptions options = new FirestoreRecyclerOptions.Builder<seller_model>()
                .setQuery(query,seller_model.class)
                .build();

        adapter = new FirestoreAdapter(this,options,this);


        //View Holder Class
        fireStoreHawkerList.setHasFixedSize(true);
        fireStoreHawkerList.setLayoutManager(new LinearLayoutManager(this));
        fireStoreHawkerList.setAdapter(adapter);

    }



    //----------------------------------------------------------------------------------------------
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(drawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }else{
            super.onBackPressed();
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch(item.getItemId()){
            case R.id.nav_profile:
                Intent profile_intent = new Intent(MainActivity.this,BuyerProfile.class);
                startActivity(profile_intent);
                break;
            case R.id.nav_logout:
                fAuth.signOut();
                Toast.makeText(this, "Loging off Users", Toast.LENGTH_SHORT).show();
                Log.d("Logout_Success","Loging off Users");
                Intent sign_out = new Intent(MainActivity.this, Login.class);
                startActivity(sign_out);
                break;
            case R.id.nav_order_history:
                Intent order_history = new Intent(MainActivity.this, OrderHistory.class);
                startActivity(order_history);
                break;
            case R.id.nav_setting:
                Intent setting_intent = new Intent(MainActivity.this,setting.class);
                startActivity(setting_intent);
                break;

        }

        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    public void onItemClick(String merchantId) {
        Intent intent = new Intent(this,Merchant.class);
        intent.putExtra("merchantId", merchantId);
        this.startActivity(intent);
    }
}

