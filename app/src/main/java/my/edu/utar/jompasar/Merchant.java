package my.edu.utar.jompasar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;

public class Merchant extends AppCompatActivity implements merchantListItemAdapter.OnListItemClick{

    private FirebaseFirestore firebaseFirestore;
    private RecyclerView merchantItemList;
    private final Context context = this;
    private ArrayList<order_menu_model> orderList = new ArrayList<order_menu_model>();
    StorageReference storeRef;
    private static Toast toastObject;
    private seller_model seller;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK && data != null) {
            if (data.hasExtra("Done")) {
                toastObject.makeText(context,"Success place order",Toast.LENGTH_SHORT ).show();
                finish();
            } else {
                this.orderList = (ArrayList<order_menu_model>) data.getSerializableExtra("orderList");
            }
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant);

        firebaseFirestore = FirebaseFirestore.getInstance();
        storeRef = FirebaseStorage.getInstance().getReference();

        toastObject = new Toast(context);
        merchantItemList = findViewById(R.id.merchantItemList);
        // Get Merchant ID from last activity
        Intent intent = getIntent();
        String merchantId = intent.getStringExtra("merchantId");
//        String merchantId = "EMm9Q4Vu6heKXlsodBa4"; // hardcode first

        // Query
        DocumentReference docRef = firebaseFirestore.collection("sellers").document(merchantId);
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                seller = documentSnapshot.toObject(seller_model.class);

                // Set the Seller Details
                TextView tvName , tvRating;
                tvName = (TextView) findViewById(R.id.merchantName);
                tvRating = (TextView) findViewById(R.id.merchantRating);
                tvName.setText(seller.getShop_name());
                tvRating.setText("\u2605" + seller.getAvg_rating().toString() + "(" + seller.getRating_count().toString() +")");

                // Set adapter
                merchantListItemAdapter adapter = new merchantListItemAdapter(context,seller.getMenu(), (merchantListItemAdapter.OnListItemClick) context);
                merchantItemList.setAdapter(adapter);
                merchantItemList.setLayoutManager(new LinearLayoutManager(context));
                merchantItemList.setHasFixedSize(false);

                // Set icon function
                ImageButton detailIcon = (ImageButton) findViewById(R.id.detailIcon);
                detailIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, MerchantDetail.class);
                        intent.putExtra("seller", (Serializable) seller);
                        intent.putExtra("merchantId", merchantId);
                        startActivity(intent);
                    }
                });
                ImageButton backIcon = (ImageButton) findViewById(R.id.backIcon);
                backIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
                if (null != toastObject){
                    toastObject.cancel();
                }
                toastObject.makeText(context, adapter.getItemCount() + " items found", Toast.LENGTH_SHORT ).show();
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (null != toastObject){
                    toastObject.cancel();
                }
                toastObject.makeText(context,"Failed to load data", Toast.LENGTH_SHORT ).show();
                finish();
            }
        });

        Button checkout = (Button) findViewById(R.id.checkout);
        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Checkout.class);
                intent.putExtra("order_menu_model", (Serializable) orderList);
                intent.putExtra("seller", (Serializable) seller);
                intent.putExtra("merchantId",merchantId);
                startActivityForResult(intent, 1);
            }
        });

        // Set merchant image
        StorageReference merchantImgRef = storeRef.child("seller/" + merchantId
                + "/seller_img.jpg");
        merchantImgRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                ImageView merchantIV = (ImageView) findViewById(R.id.merchantImage);
                Picasso.get().load(uri).into(merchantIV);
            }
        });

    }

    @Override
    public void onItemClick(String itemName, Double unitPrice) {
        Integer position = checkOrderExist(itemName);
        if (null != toastObject){
            toastObject.cancel();
//            Log.d("toast","Cancel");
        }
        if(position == 999999999){
            order_menu_model tempMenu = new order_menu_model(itemName,unitPrice,1);
            this.orderList.add(tempMenu);
            toastObject.makeText(context,itemName + " *1",Toast.LENGTH_LONG).show();
            //toastObject.show();

        }else{
            Integer quantity = this.orderList.get(position).getQuantity() + 1;
            this.orderList.get(position).setQuantity(quantity);
            //toastObject.setText(this.orderList.get(position).getName() + " *" +this.orderList.get(position).getQuantity().toString());
            toastObject.makeText(context,this.orderList.get(position).getName() + " *" +this.orderList.get(position).getQuantity().toString(),Toast.LENGTH_LONG).show();
            //toastObject.show();

        }

    }

    public int checkOrderExist(String itemName) {
        if( !this.orderList.isEmpty() ){
            for (int i = 0; i < this.orderList.size(); i++) {
                if (this.orderList.get(i).getName().equals(itemName)) {
                    return i;
                }
            }
        }
        return 999999999;
    }

}
