package my.edu.utar.jompasar;


import com.google.firebase.Timestamp;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class order_model implements Serializable {
    private String buyer_id;
    private Boolean is_delivery;
    private Timestamp order_time;
    private String remark;
    private String seller_id;
    private String seller_name;
    private String status;
    private Double total_price;
    private Boolean is_reviewed;
    private transient ArrayList<order_menu_model> menu_items;

    public order_model() {
    }

    public String getBuyer_id() {
        return buyer_id;
    }

    public void setBuyer_id(String buyer_id) {
        this.buyer_id = buyer_id;
    }

    public Boolean getIs_delivery() {
        return is_delivery;
    }

    public void setIs_delivery(Boolean is_delivery) {
        this.is_delivery = is_delivery;
    }

    public Timestamp getOrder_time() {
        return order_time;
    }

    public void setOrder_time(Timestamp order_time) {
        this.order_time = order_time;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSeller_id() {
        return seller_id;
    }

    public void setSeller_id(String seller_id) {
        this.seller_id = seller_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getTotal_price() {
        return total_price;
    }

    public void setTotal_price(Double total_price) {
        this.total_price = total_price;
    }

    public ArrayList<order_menu_model> getMenu_items() {
        return menu_items;
    }

    public void setMenu_items(ArrayList<order_menu_model> menu_items) {
        this.menu_items = menu_items;
    }
    public String getSeller_name() {
        return seller_name;
    }
    public void setSeller_name(String seller_name) {
        this.seller_name = seller_name;
    }

    public Boolean getIs_reviewed() {
        return is_reviewed;
    }

    public void setIs_reviewed(Boolean is_reviewed) {
        this.is_reviewed = is_reviewed;
    }

}
