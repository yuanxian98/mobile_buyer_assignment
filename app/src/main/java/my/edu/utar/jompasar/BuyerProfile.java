package my.edu.utar.jompasar;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

public class BuyerProfile extends AppCompatActivity {

    //Global Variable
    EditText buyer_profileUsername,buyer_profileEmail,buyer_profileTelno,buyer_profileAddr;
    ImageView profileImage;
    Button updateBtn,uploadProfilePicBtn,deactivateBtn;

    //Firebase Variable
    FirebaseAuth fAuth;
    FirebaseFirestore fStore;
    FirebaseUser fUser;
    StorageReference storeRef;
    String buyerUserID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buyer_profile);

        //Bind Java Code with XML Layout
        profileImage = findViewById(R.id.buyer_profile_image);
        buyer_profileEmail = findViewById(R.id.buyer_profile_email);
        buyer_profileUsername = findViewById(R.id.buyer_profile_username);
        buyer_profileTelno = findViewById(R.id.buyer_profile_phoneno);
        buyer_profileAddr=findViewById(R.id.buyer_profile_addr);

        updateBtn=findViewById(R.id.buyer_profile_update);
        uploadProfilePicBtn=findViewById(R.id.buyer_profile_picupload);
        deactivateBtn = findViewById(R.id.buyer_profile_deactivate);

        //Instantiate Firebase Obj
        fAuth =FirebaseAuth.getInstance();
        fStore =FirebaseFirestore.getInstance();
        storeRef = FirebaseStorage.getInstance().getReference();
        buyerUserID = fAuth.getCurrentUser().getUid();
        fUser =fAuth.getCurrentUser();

        DocumentReference docRef = fStore.collection("buyers").document(buyerUserID);
        docRef.addSnapshotListener(this, (documentSnapshot, e) -> {
            if(documentSnapshot.exists()){
                buyer_profileEmail.setText(documentSnapshot.getString("email"));
                buyer_profileTelno.setText(documentSnapshot.getString("tel_no"));
                buyer_profileUsername.setText(documentSnapshot.getString("user_name"));
                buyer_profileAddr.setText(documentSnapshot.getString("address"));
            }else{
                Log.d("Buyer_Profile_Error","Buyer Document doesn't Exist!");
            }
        });


        //Code to load the image when it's on the page
        StorageReference profileImgRef = storeRef.child("buyer/" + fAuth.getCurrentUser().getUid()
                                                                                + "/profile.jpg");
        profileImgRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Picasso.get().load(uri).into(profileImage);
            }
        });

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Get Details
                String buyerEmail = buyer_profileEmail.getText().toString();
                String buyerUsername = buyer_profileUsername.getText().toString();
                String buyerTelno = buyer_profileTelno.getText().toString();
                String buyerAddress = buyer_profileAddr.getText().toString();



                //Put Buyers Details to HashMap
                Map<String,Object> buyerProfile = new HashMap<>();
                buyerProfile.put("user_name",buyerUsername);
                buyerProfile.put("email",buyerEmail);
                buyerProfile.put("address",buyerAddress);
                buyerProfile.put("tel_no",buyerTelno);

                DocumentReference docRef = fStore.collection("buyers").document(buyerUserID);

                //Setting New Buyer Profile on Firestore Document
                docRef.set(buyerProfile).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(BuyerProfile.this, "User Profile is Updated!",
                                                                        Toast.LENGTH_SHORT).show();
                        Log.d("ProfileMsg_Success","User Profile is Updated!");

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(BuyerProfile.this, "Failure on Updating User Profile!",
                                                                        Toast.LENGTH_SHORT).show();
                        Log.d("ProfileMsg_Failure","Failure on Updating User Profile");
                    }
                });

                //Update the person email
                fUser.updateEmail(buyerEmail).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                Log.d("UserProfile","Email Updated");
                            }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                            Log.d("UserProfile","Email Updated Failed");
                    }
                });


            }
        });

        uploadProfilePicBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Open the Gallery
                //This will return the URL of the Image that the User choose
                Intent openGalleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(openGalleryIntent,1000);
            }
        });

        deactivateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fStore.collection("buyers").document(buyerUserID).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("UserProfile","Successfully Deleted User Account DB");
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("UserProfile","Failed to User Account DB");
                    }
                });

                StorageReference userStorage = storeRef.child("/buyer/" + buyerUserID + "/profile.jpg");
                userStorage.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("UserProfile","Successfully Deleted User Account Storage Information");
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("UserProfile","Failed Deleted User Account Storage Information");
                    }
                });
                fUser.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.d("UserProfile","Successfully Deleted User Account");
                        finish();
                    }
                });
                // Remember if using Toast in button, or on event status, get Application Context
                Toast.makeText(getApplicationContext(),"Successfully Deactivate User Profile",Toast.LENGTH_LONG).show();

            }
        });
    }

    //Let Buyer Select Picture by Opening the Gallery
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1000){
            if(resultCode == Activity.RESULT_OK){
                Uri imageURI = data.getData();
                //Testing code to see if ImageView is working
                //profileImage.setImageURI(imageURI);
                uploadImageToFirebase(imageURI);
            }
        }
    }

    //This function will upload the Images to Firebase
    private void uploadImageToFirebase(Uri imageURI) {

        StorageReference fileRef = storeRef.child("buyer/" + fAuth.getCurrentUser().getUid() + "/profile.jpg");

        //Remember URI is references to the Image on the Device
        fileRef.putFile(imageURI).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Toast.makeText(BuyerProfile.this,"Profile Image Uploaded!", Toast.LENGTH_SHORT).show();

                fileRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Picasso.get().load(uri).into(profileImage);

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(BuyerProfile.this,"Unable to Retrieve Profile Image from Database!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(BuyerProfile.this,"Fail to Upload Profile Image!", Toast.LENGTH_SHORT).show();
            }
        });

    }

}