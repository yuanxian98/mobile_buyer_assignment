package my.edu.utar.jompasar;

import java.util.ArrayList;

public class buyer_model {
    private String user_name;
    private String tel_no;
    private String email;
    private String address;
    private Double average_rating;
    private Integer rating_count;
    private ArrayList<String> ratedSeller;

    public buyer_model() {
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getTel_no() {
        return tel_no;
    }

    public void setTel_no(String tel_no) {
        this.tel_no = tel_no;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getAverage_rating() {
        return average_rating;
    }

    public void setAverage_rating(Double average_rating) {
        this.average_rating = average_rating;
    }

    public Integer getRating_count() {
        return rating_count;
    }

    public void setRating_count(Integer rating_count) {
        this.rating_count = rating_count;
    }

    public ArrayList<String> getRatedSeller() {
        return ratedSeller;
    }

    public void setRatedSeller(ArrayList<String> ratedSeller) {
        this.ratedSeller = ratedSeller;
    }
}
