package my.edu.utar.jompasar;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.ArrayList;

public class MerchantDetail extends AppCompatActivity {

    Context context = this;
    StorageReference storeRef;
    TabLayout merchantDetailTab;
    ViewPager viewPager;
    TextView shop_name;

    Button add_review;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_detail);
        storeRef = FirebaseStorage.getInstance().getReference();

        merchantDetailTab = findViewById(R.id.merchant_detail_tab);
        viewPager = findViewById(R.id.mDetail_view_pager);


        // Get Merchant ID and seller class information from last activity
        Intent intent = getIntent();
        String merchantId = intent.getStringExtra("merchantId");
//        String merchantId = "EMm9Q4Vu6heKXlsodBa4"; // hardcode first
        seller_model seller = (seller_model) getIntent().getSerializableExtra("seller");

        TextView merchantName = (TextView) findViewById(R.id.merchantName_detail);
        merchantName.setText(seller.getShop_name());

        // Set back icon function
        ImageButton backIcon = (ImageButton) findViewById(R.id.backIcon);
        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // Set merchant image
        StorageReference merchantImgRef = storeRef.child("seller/" + merchantId
                + "/seller_img.jpg");
        merchantImgRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                ImageView merchantIV = (ImageView) findViewById(R.id.merchantImage);
                Picasso.get().load(uri).into(merchantIV);
            }
        });

        // set Tab Layout
        merchantDetailTab.addTab(merchantDetailTab.newTab().setText("Details"));
        merchantDetailTab.addTab(merchantDetailTab.newTab().setText("Reviews(" + seller.getAvg_rating().toString() + ")"));
        merchantDetailTab.setTabGravity(TabLayout.GRAVITY_FILL);

        final merchantInfoAdapter adapter = new merchantInfoAdapter(getSupportFragmentManager(),this,merchantDetailTab.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(merchantDetailTab));
        merchantDetailTab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });


        Toast.makeText(context,seller.getShop_name(), Toast.LENGTH_LONG ).show();
    }
}
