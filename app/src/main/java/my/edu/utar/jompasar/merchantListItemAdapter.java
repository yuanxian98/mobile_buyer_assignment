package my.edu.utar.jompasar;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.jar.Attributes;

public class merchantListItemAdapter extends RecyclerView.Adapter<merchantListItemAdapter.MyViewHolder> {

    ArrayList<seller_menu_model> menu;
    Context context;
    OnListItemClick onListItemClick;

    public merchantListItemAdapter(Context c, ArrayList<seller_menu_model> sm, OnListItemClick onListItemClick){
        context = c;
        menu = sm;
        this.onListItemClick = onListItemClick;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.merchant_list_item,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.name.setText(menu.get(position).getName());
        holder.description.setText(menu.get(position).getDescription());
        holder.price.setText("From " + menu.get(position).getPrice());

    }

    @Override
    public int getItemCount() {
        return menu.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name, description, price;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            description = itemView.findViewById(R.id.description);
            price = itemView.findViewById(R.id.price);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            name = v.findViewById(R.id.name);
            String Name = name.getText().toString();
            price = v.findViewById(R.id.price);
            Double Price = Double.parseDouble(price.getText().toString().replace("From ",""));

            onListItemClick.onItemClick(Name,Price);
        }
    }
    public interface OnListItemClick {
        //Definition on what sort of data will be passed to MainActivity
        void onItemClick(String itemName, Double unitPrice);

    }
}
