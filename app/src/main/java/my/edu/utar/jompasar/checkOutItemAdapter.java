package my.edu.utar.jompasar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class checkOutItemAdapter extends RecyclerView.Adapter<checkOutItemAdapter.MyViewHolder> {

    ArrayList<order_menu_model> menu;
    Context context;
    OnListItemClick onListItemClick;

    public checkOutItemAdapter(Context c, ArrayList<order_menu_model> sm, OnListItemClick onListItemClick){
        context = c;
        menu = sm;
        this.onListItemClick = onListItemClick;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.checkout_page_item,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.name.setText(menu.get(position).getName());
        holder.quantity.setText(menu.get(position).getQuantity().toString());
        holder.price.setText("RM " + menu.get(position).getUnit_price() +"0");

    }

    @Override
    public int getItemCount() {
        return menu.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name, price, quantity;
        ImageButton addButton, removeButton;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            addButton = itemView.findViewById(R.id.plus);
            removeButton = itemView.findViewById(R.id.minus);
            addButton.setOnClickListener(this);
            removeButton.setOnClickListener(this);

            name = itemView.findViewById(R.id.name);
            quantity = itemView.findViewById(R.id.quantity);
            price = itemView.findViewById(R.id.price);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            String buttonType = "";
            switch (v.getId()) {
                case R.id.plus:
                    buttonType = "plus";
                    break;
                case R.id.minus:
                    buttonType = "minus";
                    break;
                default:
                    buttonType = "failed";
                    break;
            }

            String Name = name.getText().toString();
            Integer q = Integer.parseInt(quantity.getText().toString());
            if(buttonType=="plus"){
                quantity.setText(String.valueOf(q+1));
            }else if(buttonType=="minus"){
                if(q-1 <= 0){
                    View tempView = (View) v.getParent();
                    tempView.setVisibility(View.GONE);
//                    v.setVisibility(View.GONE);
                }else{
                    quantity.setText(String.valueOf(q-1));
                }
            }
            onListItemClick.onItemClick(buttonType,Name);
        }
    }

    public interface OnListItemClick {
        //Definition on what sort of data will be passed to MainActivity
        void onItemClick(String buttonType, String Name);
    }
}
