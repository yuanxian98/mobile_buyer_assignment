package my.edu.utar.jompasar;

import java.io.Serializable;

public class order_menu_model implements Serializable {
    private String id;
    private Integer quantity;
    private Double unit_price;
    private String name;

    public order_menu_model() {
    }

    public order_menu_model(String name, Double unit_price, Integer quantity) {
        this.name = name;
        this.unit_price = unit_price;
        this.quantity = quantity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(Double unit_price) {
        this.unit_price = unit_price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
