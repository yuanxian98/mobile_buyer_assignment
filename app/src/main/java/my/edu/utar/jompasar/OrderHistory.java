package my.edu.utar.jompasar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.TimeZone;

public class OrderHistory extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, orderHistoryFirestoreAdapter.OnListItemClick {

    //Global Variable
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    NavigationView navView;
    String buyerUserID;

    //Firebase Variable
    FirebaseFirestore fStore;
    FirebaseAuth fAuth;
    StorageReference storeRef;


    //Firestore UI Variable
    private RecyclerView firestoreOrderList;
    private orderHistoryFirestoreAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);

        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Kuala_Lumpur"));

        //Firebase Variable Initialization
        fAuth = FirebaseAuth.getInstance();
        fStore = FirebaseFirestore.getInstance();
        storeRef = FirebaseStorage.getInstance().getReference();
        buyerUserID = fAuth.getCurrentUser().getUid();

        //Drawer UI Logic
        drawerLayout = findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);

        navView=findViewById(R.id.nav_view);
        navView.bringToFront();

        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        navView.setNavigationItemSelectedListener(this);

        //Firestore UI RecycleView Logic
        //Query and Variable Initialization
        firestoreOrderList = findViewById(R.id.orderHistoryList);
        Query query = fStore.collection("orders" ).whereEqualTo("buyer_id",buyerUserID).orderBy("order_time", Query.Direction.DESCENDING);
//        Query query = fStore.collection("orders" );
        //Recycler Options
        FirestoreRecyclerOptions options = new FirestoreRecyclerOptions.Builder<order_model>()
                .setQuery(query,order_model.class)
                .build();

        adapter = new orderHistoryFirestoreAdapter(this,options, this);


        //View Holder Class
        firestoreOrderList.setHasFixedSize(true);
        firestoreOrderList.setLayoutManager(new LinearLayoutManager(this));
        firestoreOrderList.setAdapter(adapter);

        ImageButton backIcon = (ImageButton) findViewById(R.id.backIcon);
        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }



    //----------------------------------------------------------------------------------------------
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(drawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }else{
            super.onBackPressed();
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch(item.getItemId()){
            case R.id.nav_profile:
                Intent profile_intent = new Intent(OrderHistory.this,BuyerProfile.class);
                startActivity(profile_intent);
                break;
            case R.id.nav_logout:
                fAuth.signOut();
                Toast.makeText(this, "Loging off Users", Toast.LENGTH_SHORT).show();
                Log.d("Logout_Success","Loging off Users");
                Intent sign_out = new Intent(OrderHistory.this, Login.class);
                startActivity(sign_out);
                break;
            case R.id.nav_order_history:
                break;

            case R.id.nav_setting:
                Intent setting_intent = new Intent(OrderHistory.this,setting.class);
                startActivity(setting_intent);
                break;
        }

        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    public void onItemClick(String orderId) {
        Intent intent = new Intent(this,OrderHistoryDetail.class);
        intent.putExtra("orderId", orderId);
        this.startActivity(intent);
    }
}