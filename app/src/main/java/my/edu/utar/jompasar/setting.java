package my.edu.utar.jompasar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class setting extends AppCompatActivity {

    private RadioGroup radioGroupTheme;
    private RadioGroup radioGroupLang;
    private String theme;
    private String lang;

    private RadioButton radio_light, radio_dark;
    private RadioButton radio_lang_chi, radio_lang_eng, radio_lang_mly;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        //XML to Java Bind
        radio_light= findViewById(R.id.radioButton_light);
        radio_dark = findViewById(R.id.radioButton_dark);
        radio_lang_chi = findViewById(R.id.radioButton_chinese);
        radio_lang_eng = findViewById(R.id.radioButton_english);
        radio_lang_mly= findViewById(R.id.radioButton_Malay);

        //Check Previous state from SharedPreferences
        radio_light.setChecked(loadRadioButtons("light_theme"));
        radio_dark.setChecked(loadRadioButtons("dark_theme"));
        radio_lang_chi.setChecked(loadRadioButtons("lang_chi"));
        radio_lang_eng.setChecked(loadRadioButtons("lang_eng"));
        radio_lang_mly.setChecked(loadRadioButtons("lang_mly"));

        //Listen to changes and save state to SharedPreferences
        radio_light.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                saveRadioButtons("light_theme",isChecked);
            }
        });

        radio_dark.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                saveRadioButtons("dark_theme",isChecked);
            }
        });


        radio_lang_chi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                saveRadioButtons("lang_chi",isChecked);
            }
        });


        radio_lang_eng.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                saveRadioButtons("lang_eng",isChecked);

            }
        });

        radio_lang_mly.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                saveRadioButtons("lang_mly",isChecked);
            }
        });


    }


    public void saveRadioButtons(String key, Boolean value){
       SharedPreferences setting = getSharedPreferences("JomPasar_Setting",MODE_PRIVATE);
       SharedPreferences.Editor editor = setting.edit();
       editor.putBoolean(key,value);
       editor.apply();
    }

    public boolean loadRadioButtons(String key){
        SharedPreferences setting = getSharedPreferences("JomPasar_Setting",MODE_PRIVATE);
        return setting.getBoolean(key,false);
    }
}