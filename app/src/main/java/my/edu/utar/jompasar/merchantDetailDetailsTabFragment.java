package my.edu.utar.jompasar;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class merchantDetailDetailsTabFragment extends Fragment {
    TextView city,desc, address,email,tel_no,avg_rating;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.merchant_detail_details_tab, container, false);

        seller_model seller = (seller_model) getActivity().getIntent().getSerializableExtra("seller");
        desc = root.findViewById(R.id.desc);
        city = root.findViewById(R.id.city);
        address = root.findViewById(R.id.address);
        email = root.findViewById(R.id.email);
        tel_no = root.findViewById(R.id.tel_no);
        avg_rating = root.findViewById(R.id.avg_rating);


        desc.setText(Html.fromHtml("<b>Description</b>: \n" + seller.getDesc()));
        city.setText(Html.fromHtml("<b>City</b>: \n" + seller.getCity()));
        address.setText(Html.fromHtml("<b>Address</b>: \n" + seller.getAddress()));
        email.setText(Html.fromHtml("<b>Email</b>: \n" + seller.getEmail()));
        tel_no.setText(Html.fromHtml("<b>Phone</b>: \n" + seller.getTel_no()));
        avg_rating.setText(Html.fromHtml("<b>Rating</b>: \n" + seller.getAvg_rating().toString()));


        return root;
    }
}
