package my.edu.utar.jompasar;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.HashMap;

public class OrderHistoryDetail extends AppCompatActivity {

    //Firebase Variable Initialization
    private FirebaseFirestore firebaseFirestore;
    private Context context = this;
    FirebaseAuth fAuth;
    StorageReference storeRef;
    DocumentReference reviewRef;
    DocumentReference buyerRef;

    //UI Variable for hooks
    AlertDialog.Builder rate_alert;
    LayoutInflater inflater;
    RadioGroup rating_group;
    Button rateButton;

    //Global Variable
    String buyerID;
    buyer_model buyer;
    order_model order;
    seller_model seller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history_detail);

        //Firebase UI Initialization
        firebaseFirestore = FirebaseFirestore.getInstance();
        fAuth = FirebaseAuth.getInstance();
        storeRef = FirebaseStorage.getInstance().getReference();
        reviewRef = firebaseFirestore.collection("reviews").document();
        buyerID = fAuth.getCurrentUser().getUid();

        //Variable Initialization
        Intent intent = getIntent();
        String orderId = intent.getStringExtra("orderId");

        //UI Initialization
        rate_alert = new AlertDialog.Builder(this);
        inflater = this.getLayoutInflater();

        //Rate Button Logic:
        rateButton = (Button) findViewById(R.id.rateButton);

        buyerRef = firebaseFirestore.collection("buyers").document(buyerID);
        buyerRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot snapshot) {
                buyer =snapshot.toObject(buyer_model.class);
            }
        });

        // Query - to get particular order
        DocumentReference docRef = firebaseFirestore.collection("orders").document(orderId);
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {

                //Convert Firebase Document to POJO
                order = documentSnapshot.toObject(order_model.class);
                TextView merchantName, orderItem, quantity, unitPrice, totalPrice, remark, status, delivery;

                //XML Binding and Display Details Code
                merchantName = (TextView) findViewById(R.id.merchantName);
                orderItem = (TextView) findViewById(R.id.orderItem);
                quantity = (TextView) findViewById(R.id.quantity);
                unitPrice = (TextView) findViewById(R.id.unit_price);
                totalPrice = (TextView) findViewById(R.id.totalPrice);
                remark = (TextView) findViewById(R.id.remark);
                status = (TextView) findViewById(R.id.status);
                delivery = (TextView) findViewById(R.id.delivery);

                merchantName.setText(order.getSeller_name());
                totalPrice.setText("Total: RM" + order.getTotal_price() + "0");
                if(order.getRemark()!=null){
                    remark.setText("Remark: " + order.getRemark());
                }
                status.setText(order.getStatus());
                if(order.getIs_delivery()){
                    delivery.setText("This is a delivery order");
                }

                String orderItemString = "";
                String quantityString = "";
                String unitPriceString = "";
                ArrayList<order_menu_model> order_items = order.getMenu_items();
                for (int i = 0; i < order_items.size(); i++) {
                    order_menu_model order_item = order_items.get(i);
                    orderItemString += order_item.getName() + "\n";
                    unitPriceString += "RM " + order_item.getUnit_price() + "0\n";
                    quantityString += "x" + order_item.getQuantity() + "\n";
                }
                orderItem.setText(orderItemString);
                quantity.setText(quantityString);
                unitPrice.setText(unitPriceString);

                DocumentReference sellerRef = firebaseFirestore.collection("sellers").document(order.getSeller_id());
                sellerRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot snapshot) {
                        seller = snapshot.toObject(seller_model.class);
                    }
                });

                if(buyer.getRatedSeller() != null){
                    if (buyer.getRatedSeller().contains(order.getSeller_id())) {
                        rateButton.setVisibility(View.GONE);
                    }else{
                        setRateButton();
                    }
                }else{
                    setRateButton();
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(context, "Failed to load data", Toast.LENGTH_LONG).show();
                finish();
            }

        });

        // Set back icon function
        ImageButton backIcon = (ImageButton) findViewById(R.id.backIcon);
        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    private void setRateButton(){
        rateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = inflater.inflate(R.layout.rate_popup, null);
                rate_alert.setTitle("Leave a Review?")
                        .setMessage("Please enter your Rating and Review according to seller")
                        .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Get the Comment from dialog box
                                EditText comment = view.findViewById(R.id.rate_popup_review);
                                String review_comment = comment.getText().toString();

                                //Get Rating from Radio Button Group
                                rating_group = view.findViewById(R.id.rating_radio_group);
                                RadioButton rating_1 = view.findViewById(R.id.rating_1);
                                RadioButton rating_2 = view.findViewById(R.id.rating_2);
                                RadioButton rating_3 = view.findViewById(R.id.rating_3);
                                RadioButton rating_4 = view.findViewById(R.id.rating_4);
                                RadioButton rating_5 = view.findViewById(R.id.rating_5);

                                int selectedId = rating_group.getCheckedRadioButtonId();
                                RadioButton radioSelectedbutton = (RadioButton) view.findViewById(selectedId);
                                String input = radioSelectedbutton.getText().toString();

                                Integer rating=Integer.parseInt(input);

                                //Pack the info into obj and push it to Firestore
                                HashMap<String, Object> review = new HashMap<>();
                                review.put("buyer_id", order.getBuyer_id());
                                review.put("buyer_name", buyer.getUser_name());
                                review.put("comment", review_comment);
                                review.put("rating", rating);
                                review.put("seller_id", order.getSeller_id());
                                review.put("timestamp", FieldValue.serverTimestamp() );
                                review.put("type","seller");
                                ArrayList<String> ratedSeller = new ArrayList<String>();
                                if(buyer.getRatedSeller() != null){
                                    ratedSeller = (ArrayList<String>) buyer.getRatedSeller();
                                }
                                ratedSeller.add(order.getSeller_id());
                                buyer.setRatedSeller(ratedSeller);
                                firebaseFirestore.collection("buyers").document(buyerID).set(buyer);

                                Integer ratingCount = seller.getRating_count() +1;
                                seller.setRating_count(ratingCount);
                                firebaseFirestore.collection("sellers").document(order.getSeller_id()).set(seller);

                                reviewRef.set(review).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(getApplicationContext(),"Successfully upload Rating to Database",Toast.LENGTH_LONG).show();
                                        rateButton.setVisibility(View.GONE);
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(getApplicationContext(),"Failed to upload Rating to Database",Toast.LENGTH_LONG).show();
                                    }
                                });

                            }
                        }).setNegativeButton("Cancel", null).setView(view).create().show();
            }
        });
    }
}