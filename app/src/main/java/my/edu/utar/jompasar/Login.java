package my.edu.utar.jompasar;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Login extends AppCompatActivity {
    EditText buyer_loginEmail, buyer_loginPassword;
    Button buyer_loginBtn, buyer_loginResetPass, buyer_signUpBtn;
    FirebaseAuth login_fAuth;
    AlertDialog.Builder ForgetPass_Alert;
    LayoutInflater inflater;
    FirebaseUser user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        buyer_loginEmail = findViewById(R.id.buyer_loginEmail);
        buyer_loginPassword = findViewById(R.id.buyer_loginPassword);
        buyer_loginBtn = findViewById(R.id.buyer_loginBtn);
        buyer_signUpBtn = findViewById(R.id.buyer_signupBtn);
        buyer_loginResetPass = findViewById(R.id.buyer_loginResetBtn);

        login_fAuth=FirebaseAuth.getInstance();
        user = login_fAuth.getCurrentUser();
        if(user!=null){
            Toast.makeText(Login.this,"Login Successfully",Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getApplicationContext(),MainActivity.class));
        }
        ForgetPass_Alert = new AlertDialog.Builder(this);
        inflater = this.getLayoutInflater();

        //Button Logic on Buyer Login Activity
        buyer_loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Validate Details
                if(buyer_loginEmail.getText().toString().isEmpty()){
                    buyer_loginEmail.setError("Email is Missing");
                    return;
                }

                if(buyer_loginPassword.getText().toString().isEmpty()){
                    buyer_loginPassword.setError("Password is Missing");
                    return;
                }

                //Extract Details
                String buyerEmail = buyer_loginEmail.getText().toString();
                String buyerUserPass = buyer_loginPassword.getText().toString();

                //Login in Logic
                login_fAuth.signInWithEmailAndPassword(buyerEmail,buyerUserPass).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        Toast.makeText(Login.this,"Login Successfully",Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(),MainActivity.class));
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(Login.this,"Login Failure",Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        buyer_signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Sent it to Sign Up Activity
                startActivity(new Intent(getApplicationContext(),signup.class));
            }
        });

        buyer_loginResetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Start Alert Dialog
                View view =inflater.inflate(R.layout.reset_popup,null);
                ForgetPass_Alert.setTitle("Reset Password?")
                        .setMessage("Enter your Registered Email to get the Reset Link")
                        .setPositiveButton("Reset", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Validate
                                EditText email = view.findViewById(R.id.reset_email_popup);
                                if(email.getText().toString().isEmpty()){
                                    email.setError("This Field is Required");
                                    return;
                                }
                                //Send the Reset Link
                                login_fAuth.sendPasswordResetEmail(email.getText().toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(Login.this,"Email Sent!",Toast.LENGTH_SHORT).show();
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(Login.this,e.getMessage(),Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        }).setNegativeButton("Cancel",null).setView(view).create().show();

            }
        });
    }



    @Override
    protected void onStart() {
        super.onStart();
        //Check If the User Have Previously Logged In or Not
        //if(FirebaseAuth.getInstance().getCurrentUser()!=null){
            //If Logged in, Then Send them to Landing Page
            //Debug Xia, I think right now there's no way for user to log out, so it will keep switch to that
            //startActivity(new Intent(getApplicationContext(),MainActivity.class));
        //}
    }


}