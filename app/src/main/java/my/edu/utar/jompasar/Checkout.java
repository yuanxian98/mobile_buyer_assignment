package my.edu.utar.jompasar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

public class Checkout extends AppCompatActivity implements checkOutItemAdapter.OnListItemClick {

    private RecyclerView checkoutList;
    private final Context context = this;
    private ArrayList<order_menu_model> orderList = new ArrayList<order_menu_model>();
    private checkOutItemAdapter adapter;
    private Double totalPrice;
    private TextView tvPrice;
    EditText remarkEdit;
    Switch deliverySwitch;

    FirebaseFirestore fStore;
    FirebaseAuth fAuth;
    StorageReference storeRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        Intent intent = getIntent();
        orderList = (ArrayList<order_menu_model>) intent.getSerializableExtra("order_menu_model");
        String merchantId = intent.getStringExtra("merchantId");

        if( this.orderList.isEmpty() ){
            findViewById(R.id.placeOrder).setEnabled(false);
        }else{
            calcTotalPrice();
        }

        checkoutList = (RecyclerView) findViewById(R.id.checkoutItemList);

        adapter = new checkOutItemAdapter(context,orderList, (checkOutItemAdapter.OnListItemClick)context);
        checkoutList.setAdapter(adapter);
        checkoutList.setLayoutManager(new LinearLayoutManager(context));
        checkoutList.setHasFixedSize(false);

        remarkEdit = findViewById(R.id.remark);
        deliverySwitch = findViewById(R.id.delivery);

        Button placeOrder = (Button) findViewById(R.id.placeOrder);
        placeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fAuth = FirebaseAuth.getInstance();
                fStore = FirebaseFirestore.getInstance();
                storeRef = FirebaseStorage.getInstance().getReference();
                String buyerUserID = fAuth.getCurrentUser().getUid();
                String remark = remarkEdit.getText().toString();
                seller_model seller = (seller_model) getIntent().getSerializableExtra("seller");
                order_model order = new order_model();
                order.setBuyer_id(buyerUserID);
                order.setMenu_items(orderList);
                order.setSeller_id(merchantId);
                order.setSeller_name(seller.getShop_name());
                order.setStatus("pending");
                order.setRemark(remark);
                order.setTotal_price(totalPrice);
                order.setIs_reviewed(false);
                if(deliverySwitch.isChecked()==true){
                    order.setIs_delivery(true);
                }else{
                    order.setIs_delivery(false);
                }
                TimeZone.setDefault(TimeZone.getTimeZone("Asia/Kuala_Lumpur"));
                order.setOrder_time(new Timestamp(new Date()));
                fStore.collection("orders").add(order);


                Intent output = new Intent();
                output.putExtra("Done","1");
                setResult(RESULT_OK, output);
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent output = new Intent();
        output.putExtra("orderList",(Serializable) this.orderList);
        setResult(RESULT_OK, output);
        finish();
    }

    @Override
    public void onItemClick(String buttonType, String itemName) {
        Integer position = checkOrderExist(itemName);

        if(buttonType == "plus"){
            Integer quantity = this.orderList.get(position).getQuantity() + 1;
            this.orderList.get(position).setQuantity(quantity);
            calcTotalPrice();
        }else if(buttonType == "minus"){
            if(position == 999999999){
                Log.d("gg","Error, null pointer");
            }
            Integer quantity = this.orderList.get(position).getQuantity() - 1;
            if(quantity<=0){
                order_menu_model tempMenu = this.orderList.get(position);
                this.orderList.remove(tempMenu);
                checkoutList.removeViewAt(position);
                adapter.notifyItemRemoved(position);
                adapter.notifyItemRangeChanged(position, this.orderList.size());
                adapter.notifyDataSetChanged();
                if(orderList.size()<=0){
                    Intent output = new Intent();
                    output.putExtra("orderList",(Serializable) this.orderList);
                    setResult(RESULT_OK, output);
                    finish();
                }
                calcTotalPrice();
            }else{
                this.orderList.get(position).setQuantity(quantity);
                calcTotalPrice();
            }
        }

    }
    public int checkOrderExist(String itemName) {
        if( !this.orderList.isEmpty() ){
            for (int i = 0; i < this.orderList.size(); i++) {
                if (this.orderList.get(i).getName().equals(itemName)) {
                    return i;
                }
            }
        }
        return 999999999;
    }

    public void calcTotalPrice(){
        totalPrice = 0.00;
        for (int i = 0; i < this.orderList.size(); i++) {
            totalPrice += orderList.get(i).getUnit_price() * orderList.get(i).getQuantity();
        }
        tvPrice=findViewById(R.id.totalPrice);
        tvPrice.setText("Total Price: RM" + totalPrice);
    }
}